#!/usr/bin/bash

show_help(){
printf 'List information about the University of São Paulo restaurants
Usage: lsru [-w] [-l|-d]
Options:
    -h, --help       print this help message and exit
    -l, --lunch      print the menu for lunch
    -d, --dinner     print the menu for dinner
    -w, --week       print the menus for all days of the week\n'
}

MONTH=$(date +"%m" | sed 's/^0//')
DAY=$(date +"%d" | sed 's/^0//')
HOUR=$(date +"%H" | sed 's/^0//')
MINUTE=$(date +"%M" | sed 's/^0//')

if [ "$HOUR" -lt 13 ] || ([ "$HOUR" -eq 13 ] && [ "$MINUTE" -le 30 ]); then
    BEFORE_L=true
    BEFORE_D=true
elif [ "$HOUR" -lt 19 ] || ([ "$HOUR" -eq 19 ] && [ "$MINUTE" -le 30 ]); then
    BEFORE_L=false
    BEFORE_D=true
elif [ "$HOUR" -lt 23 ] || ([ "$HOUR" -eq 23 ] && [ "$MINUTE" -le 59 ]); then
    DAY=$((${DAY}+1))
    BEFORE_L=true
    BEFORE_D=true
fi

ALL_WEEK=false
while [ -n "$1" ]; do
    case "$1" in
        -h | --help)
            show_help
            exit 0;;
        -l | --lunch)
            BEFORE_L=true
            shift
            if [ -n "$1" ]; then
                case "$1" in
                    --dinner)
                        BEFORE_D=true;;
                esac
            else
                BEFORE_D=false
            fi;;
        -d | --dinner)
            BEFORE_L=false
            shift
            if [ -n "$1" ]; then
                case "$1" in
                    --lunch)
                        BEFORE_L=true;;
                esac
            else
                BEFORE_L=false
            fi;;
        -ld | -dl)
            BEFORE_L=true
            BEFORE_D=true;;
        -w | --week)
            ALL_WEEK=true
            BEFORE_L=true
            BEFORE_D=true;;
        -- )
            shift
            break;;
        *)
            printf "$1 is not an option!"
            exit 1;;
    esac
    shift
done

JSON=$(curl -s 'https://uspdigital.usp.br/rucard/dwr/call/plaincall/CardapioControleDWR.obterCardapioRestUSP.dwr' --data-raw $'callCount=1\nwindowName=\nnextReverseAjaxIndex=0\nc0-scriptName=CardapioControleDWR\nc0-methodName=obterCardapioRestUSP\nc0-id=0\nc0-param0=string:19\nbatchId=1\ninstanceId=0\npage=%2Frucard%2FJsp%2FcardapioSAS.jsp%3Fcodrtn%3D19\nscriptSessionId=\n' \
        | sed -e 's/^[^\[]*//' \
        -e 's/\][^\[]*\[/\] \[/g' \
        -e 's/[^]]*$//' \
        -e 's/^[^\{]*//' \
        -e 's/^/\[/' \
        -e 's/^\[$//g' \
        -e 's/cdpdia/"cdpdia"/g' \
        -e 's/codddd1/"codddd1"/g' \
        -e 's/codrtn/"codrtn"/g' \
        -e 's/diames/"diames"/g' \
        -e 's/diasemana/"diasemana"/g' \
        -e 's/dtainismncdp/"dtainismncdp"/g' \
        -e 's/dtarfi/"dtarfi"/g' \
        -e 's/nomrtn/"nomrtn"/g' \
        -e 's/numtel1/"numtel1"/g' \
        -e 's/obscdp:/"obscdp":/g' \
        -e 's/obscdpsmn/"obscdpsmn"/g' \
        -e 's/tiprfi/"tiprfi"/g' \
        -e 's/vlrclorfi/"vlrclorfi"/g' \
        -e 's/<br>/\\n/g')

menu(){
    DAY="$1"
    printf "\n\t\t$DAY\n\n"
    $BEFORE_L && echo "[33mLunch:(B[m" && echo "${JSON}" | jq --arg day "${DAY}" -r '.[] | select((.diames == ($day | tonumber)) and (.tiprfi == "A")) | {cdpdia} | join(" ")'
    $BEFORE_L && $BEFORE_D && printf "\n"
    $BEFORE_D && echo "[33mDinner:(B[m" && echo "${JSON}" | jq --arg day "${DAY}" -r '.[] | select((.diames == ($day | tonumber)) and (.tiprfi == "J")) | {cdpdia} | join(" ")'
}

if $ALL_WEEK; then
    DAYS_OF_WEEK=$(cal -v3 | sed -Ee 's/\ {8}/\ /g' -e 's/^\ {4}[A-Z].*//g' -e 's/^.{15}//g' -e 's/.{27}$//g')
    for DAY in $DAYS_OF_WEEK; do
        menu $DAY
    done
else
    menu $DAY
fi
