{
  description = "Lsru";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";
  };

  outputs = { self, nixpkgs }:
    let
      forAllSystems = nixpkgs.lib.genAttrs nixpkgs.lib.systems.flakeExposed;
      pkgsFor = forAllSystems (system: import nixpkgs {
        inherit system;
        overlays = builtins.attrValues self.overlays;
      });
    in
    {
      overlays = rec {
        default = _final: prev: {
          lsru = prev.callPackage ./. { };
        };
      };

      packages = forAllSystems (s:
        let pkgs = pkgsFor.${s}; in
        rec {
          inherit (pkgs) lsru;
          default = lsru;
        });
    };
}
