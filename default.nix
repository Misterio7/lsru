{ pkgs ? import <nixpkgs> { }
, lib ? pkgs.lib
, stdenv ? pkgs.stdenv
, jq ? pkgs.jq
, ...
}: stdenv.mkDerivation rec {
  pname = "lsru";
  version = builtins.elemAt (builtins.match ".*pkgver=([^\n]*).*" (builtins.readFile ./PKGBUILD)) 0;
  src = ./.;

  configurePhase = ''
    sed -e 's#jq#${jq}/bin/jq#' -i lsru.sh
  '';
  dontBuild = true;
  installPhase = ''
    install -Dm 0755 lsru.sh $out/bin/lsru
  '';
}
